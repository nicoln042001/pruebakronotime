<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KronoTime</title>
    <link rel="stylesheet" href="{{ asset('css/global.css') }}">
    <script src="https://kit.fontawesome.com/ccb913ffc7.js" crossorigin="anonymous"></script>
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<header>

    <nav class="navbar navbar-expand-lg navbar-dark menu">
        <img src="images/icono.PNG" alt="">
        <a class="navbar-brand" href="#">TechnoMarin</a>
        <a class="btn-menu d-block d-sm-block d-md-none text-light"><i class="fas fa-bars"></i></a>

        <div class="collapse navbar-collapse text-right" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <div class="input-group flex-nowrap">
                    <input type="text" class="form-control buscar" placeholder="Username" aria-label="Username" aria-describedby="addon-wrapping">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-white icono" id="addon-wrapping"><i class="fas fa-search"></i></span>
                    </div>
                </div>
                <li class="nav-item active">
                    <a class="nav-link" href="#"><i class="fas fa-shopping-cart"></i></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#"><i class="fas fa-phone-volume"></i></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#"><i class="far fa-calendar"></i></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#"><i class="fas fa-map-marker-alt"></i></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#"><i class="fas fa-globe-americas"></i></a>
                </li>
                <li class="nav-item active user">
                    <a class="nav-link" href="#">INICIAR SESIÓN <i class="fas fa-user"></i></a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="segundo-menu">
        <ul>
            <div class="row text-right">
                <li class="nav-item col-md-6">
                    <a class="nav-link" href="#">COLECCIONES</a>
                </li>
                <li class="nav-item col-md-2">
                    <a class="nav-link" href="#">HOMBRE</a>
                </li>
                <li class="nav-item col-md-2">
                    <a class="nav-link" href="#">MUJER</a>
                </li>
                <li class="nav-item col-md-2">
                    <a class="nav-link" href="#">OFERTAS</a>
                </li>
            </div>

        </ul>
    </div>

    <div class="botonera-resposive">
        <a class="btn-menu text-light"><i class="fas fa-times-circle"></i></a>
        <table class="table table-striped">
            <tbody>
                <tr>
                    <td>
                        <li class="nav-item active">
                            <a class="nav-link" href="#"><i class="fas fa-shopping-cart"></i></a>
                            <a class="nav-link" href="#"><i class="fas fa-phone-volume"></i></a>
                            <a class="nav-link" href="#"><i class="far fa-calendar"></i></a>
                            <a class="nav-link" href="#"><i class="fas fa-map-marker-alt"></i></a>
                            <a class="nav-link" href="#"><i class="fas fa-globe-americas"></i></a>
                        </li>
                    </td>
                </tr>
                <tr>
                    <td>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">INICIAR SESIÓN <i class="fas fa-user"></i></a>
                        </li>
                    </td>
                </tr>
                <tr>
                    <td>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">COLECCIONES</a>
                        </li>
                    </td>

                </tr>
                <tr>
                    <td>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">HOMBRE</a>
                        </li>
                    </td>
                </tr>
                <tr>
                    <td>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">MUJER</a>
                        </li>
                    </td>
                </tr>
                <tr>
                    <td>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">OFERTAS</a>
                        </li>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</header>

<body>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="images/50off-desk.jpg" class="d-block w-100" alt="">
            </div>
            <div class="carousel-item">
                <img src="images/70off-desk.jpg" class="d-block w-100" alt="">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="divisor ml-auto mt-2 mb-2">
        <h4 class="">OFERTAS ESPECIALES</h4>
    </div>
    <div class="container">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj1.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj1.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj1.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj2.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj2.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj2.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="banner">
        <img src="images/hola.jpg" alt="" class="imagen2">
        <img src="images/techno11.jpg" alt="" class="imagen1">
    </div>

    <div class="divisor ml-auto mt-2 mb-2">
        <h4 class="">COLECCIÓN DEL MES</h4>
    </div>
    <div class="segundo-banner">
        <img src="images/banner-manta-collection-01.jpg" alt="">
    </div>
    <div class="container mt-2">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj4.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj4.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj4.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj3.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj3.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                        <div class="col-md-4 item-carrusel text-center">
                            <img src="images/reloj3.jpg" class="d-block w-100" alt="">
                            <span>
                                <p>Reloj Technomarine Manta TM-215064</p>
                                <p class="text-danger">$599.900 </p>
                            </span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="divisor ml-auto mt-4 mb-2">
        <h4 class="">CONVIERTETE EN UN FAN DE TECHNOMARINE</h4>
    </div>
    <div class="col-md-12">
        <p>INSCRÍBETE A NUESTRO NEWSLETTER Y OBTEN UN 10% DE DESCUENTO EN TUS COMPRAS ONLINE</p>
        <div class="row">
            <div class="col-md-10">
                <input type="text" class="form-control" placeholder="Tu correo electrónico">
            </div>
            <div class="col-md-2">
                <button class="btn btn-dark">Enviar</button>
            </div>
            <input class="form-check-label ml-3 mr-2" type="checkbox" name="" id=""><span>Al ingresar mi correo acepto <strong>los términos y condiciones</strong></span>
        </div>
    </div>

</body>
<footer>
    <div class="container mb-5 mt-3">
        <div class="row">
            <div class="col-md-2">
                <span>CUIDA TU RELOJ</span>
            </div>
            <div class="col-md-2">
                <span>PQRS</span>
            </div>
            <div class="col-md-2">
                <span>CONTACTOS</span>
            </div>
            <div class="col-md-2">
                <span>CONDICIONES</span>
            </div>
            <div class="col-md-2">
                <span>MI CUENTA</span>
            </div>
            <div class="col-md-2">
                <span>NOSOTROS</span>
            </div>
        </div>
    </div>
    <div class="col-md-12 mb-5">
        <img src="images/technomarine_logo.jpg" alt="" class="m-auto d-block">
    </div>

    <div class="bg-dark text-light derechos">
        Todos los derechos reservados TechnoMarine 2019
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

</html>